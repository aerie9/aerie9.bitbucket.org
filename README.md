# README #

Online repo for learning D3.js

Samples so far:

* Basic page: aerie9.bitbucket.org/basic.html
* Bar-chart: aerie9.bitbucket.org/bar-chart.html

See Reddit post: 
https://www.reddit.com/r/datasciencestudygroup/comments/5f8mil/learning_d3js/

Well recommended tutorials I've seen online:

* Scott Murray's D3 Tutorials http://alignedleft.com/tutorials/d3/
* D3 in Depth http://d3indepth.com
* D3 GitHub Home https://github.com/d3/d3/wiki
* Dashing D3.js https://www.dashingd3js.com/lessons
* Zero Viscosity http://zeroviscosity.com/category/d3-js-step-by-step

More about D3:

* Mike Bostock's Reddit AMA https://www.reddit.com/r/dataisbeautiful/comments/3k3if4/hi_im_mike_bostock_creator_of_d3js_and_a_former/

Examples:

* https://bl.ocks.org
* https://github.com/d3/d3/wiki/Gallery